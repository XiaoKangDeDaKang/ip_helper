<?php

namespace Dgyun\IpHelper;

use Dgyun\IpHelper\Ip2Region\Ip2RegionHelper;

class Ip
{

    function location($ip)
    {
        return (new Ip2RegionHelper())->searchIp2Region($ip);
    }


    function get_client_ip()
    {
        if (Arr::get($_SERVER, 'X-Real-IP')) {
            $ip = $_SERVER['X-Real-IP'];
        } elseif (Arr::get($_SERVER, 'X-Forwarded-For')) {
            $ip = $_SERVER['X-Forwarded-For'];
        } elseif (Arr::get($_SERVER, 'X_FORWARDED_FOR')) {
            $ip = $_SERVER['X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'] ?? '0.0.0.0';
        }

        // 如果是外网IP，则直接返回
        if (boolval(filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE))) {
            return $ip;
        }

        // 代理头
        $proxies = [
            'HTTP_X_FORWARDED_FOR',
            'HTTP_FORWARDED_FOR',
            'HTTP_X_FORWARDED',
            'HTTP_FORWARDED',
            'HTTP_X_REAL_IP',
            'HTTP_CLIENT_IP',
        ];

        // 从指定的HTTP头中依次尝试获取IP地址
        // 直到获取到一个合法的IP地址
        foreach ($proxies as $proxy) {
            $temp = getenv($proxy);

            if (empty($temp)) {
                continue;
            }

            // 验证格式
            $temp = trim(explode(',', $temp)[0]);
            if (boolval(filter_var($temp, FILTER_VALIDATE_IP))) {

                // 如果是外网IP，则直接返回
                if (boolval(filter_var($temp, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE))) {
                    return $temp;
                }
            }
        }

        return $ip;
    }


    function get_client_ip_tencent_cdn()
    {
        $ip = '';
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = trim($ips[0]);
        } elseif (isset($_SERVER['HTTP_X_FORWARDED'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED'];
        } elseif (isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_FORWARDED'])) {
            $ip = $_SERVER['HTTP_FORWARDED'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    function get_client_ip_ali_cdn()
    {
        $ip = '';
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip  = trim($ips[0]);
        } elseif (isset($_SERVER['HTTP_X_FORWARDED'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED'];
        } elseif (isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_FORWARDED'])) {
            $ip = $_SERVER['HTTP_FORWARDED'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
}