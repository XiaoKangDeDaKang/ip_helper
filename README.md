本库IP地址是基于 [ip2region](https://github.com/lionsoul2014/ip2region) 简单整合，方便 `PHP` 项目使用 `Composer` 来安装。

# 通过 Composer 安装

```shell
composer require dgyun/ip_helper
```

# 在项目中快速调用

```php
use Dgyun\IpHelper\Ip;

$result = (new Ip())->location('0.0.0.0');

var_dump($result);

```

# 获取客户端ip

```php
use Dgyun\IpHelper\Ip;

$result = (new Ip())->get_client_ip();

$result = (new Ip())->get_client_ip_ali_cdn();

$result = (new Ip())->get_client_ip_tencent_cdn();
var_dump($result);

```
